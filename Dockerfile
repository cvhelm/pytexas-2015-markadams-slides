FROM node:0.12

EXPOSE 8000
RUN npm install -g grunt-cli
WORKDIR /usr/src/app

COPY package.json package.json
RUN npm install .

COPY . .

CMD ["grunt", "serve"]
